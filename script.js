
const url = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

const getData = function () {
  Promise.all([
    fetch(url)  .then((response) => {
      return response.json();
    }),
    fetch(urlPosts)  .then((response) => {
      return response.json();
    }),
  ]).then(allResponses => {
    const users = allResponses[0]
    const posts = allResponses[1]
    createCard(users, posts)
  })
}

const createCard = function (users, posts) {
         const findId = users.map((element) => {
          const {id, name, email, username} = element;
          return {id, name, email, username}
    })
      const postsCardData = posts.map((element) => {
      const user = findId.find(({ id }) => {
        return id === element.userId;
      })
       return{ ...user, ...element  }; 
    })    
    postsCardData.forEach((el)=>{
      const{body, email, userId, name, title, username, id} = el;
      const card = new Card ( body, email, name, title, username, userId, id ) 
      card.render();
   }) 
  
  };

getData()

class Card  {
  constructor(body, email, name, title, username, userId, id ) {
    this.title = title;
    this.userId = userId;
    this.body = body;
    this.name = name;
    this.username = username;
    this.email = email;
    this.id = id;   
  }
  
  render() {
    const cardElement = document.createElement('div')
    cardElement.classList.add('card');
    cardElement.insertAdjacentHTML("afterbegin", [`
    <h2>${this.title}</h2>
    <p>${this.body}</p>
    <span>${this.name}</span>
    <span>${this.email}</span>
    <button class="removeBtn">Delete</button>`])
    cardElement.querySelector(".removeBtn").addEventListener('click', () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}` , {
        method: 'DELETE'
      })
      .then (() => {
        cardElement.remove()
        console.log(`Deleted post ${this.id}`);
       })
    })
    document.body.append(cardElement)
  }
}
